package com.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.config.DelegatingApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.context.event.EventListenerFactory;
import org.springframework.context.event.EventListenerMethodProcessor;

/**
 * springboot事件监听的流程
 * 1.自定义事件：继承ApplicationEvent抽象类
 * 2.定义事件监听器：实现ApplicationListener接口
 * 3.将事件添加到spring容器：
 * 		1.SpringApplication.addListeners 
 * 		2.使用注解：如@Component
 * 		3.使用properties配置：context.listener.classes=com.learn.MyApplicationListener,详情参照DelegatingApplicationListener
 * 		4.在方法上使用@EventListener注解，其类需要加入spring容器，详情参照：EventListenerMethodProcessor,EventListenerFactory
 * 4.发布事件：ConfigurableApplicationContext.publishEvent
 * 5.通过/springboot-children/src/main/resources/META-INF/spring.factories
 * @author Romanceling
 *
 */
@SpringBootApplication
public class App {
    public static void main( String[] args ){
    	SpringApplication app = new SpringApplication(App.class);
    	//3.添加事件
    	//app.addListeners(new MyApplicationListener());
        ConfigurableApplicationContext context = app.run(args);
        
        //4.发布事件
        context.publishEvent(new MyApplicationEvent(new Object()));
        context.close();
    }
}
