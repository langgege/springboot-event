package com.learn;

import org.springframework.context.ApplicationEvent;

/**
 * 1.定义事件
 * @author Romanceling
 *
 */
public class MyApplicationEvent extends ApplicationEvent{
	private static final long serialVersionUID = 1L;

	public MyApplicationEvent(Object source) {
		super(source);
	}
}
