package com.learn;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class MyEventHandler {

	/**
	 * 参数任意
	 * 该参数（类）事件或者参数子事件（子类）都会进入此方法
	 * @param event
	 */
	@EventListener
	public void event(MyApplicationEvent event) {
		System.out.println("接收到事件========event========="+event.getClass());
	}
}
