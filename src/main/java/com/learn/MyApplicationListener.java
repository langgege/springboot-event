package com.learn;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 2.设置事件监听器
 * @author Romanceling
 *
 */
//@Component
public class MyApplicationListener implements ApplicationListener<MyApplicationEvent>{

	@Override
	public void onApplicationEvent(MyApplicationEvent event) {
		System.out.println("接收到事件========event========="+event.getClass());
	}
}
